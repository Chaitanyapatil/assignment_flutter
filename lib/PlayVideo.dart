import 'package:assignment_app/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class PlayVideo extends StatefulWidget {
  @override
  _PlayVideoState createState() => _PlayVideoState();
}

class _PlayVideoState extends State<PlayVideo> {

  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.asset('assets/Video.mp4')..initialize().then((_) {
      _controller.setVolume(0.0);
      _controller.play();
      setState(() {
       /* Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()),
        );*/
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _controller.value.initialized ? AspectRatio(aspectRatio: _controller.value.aspectRatio,
      child: VideoPlayer(_controller),
      ): Container(),
    );
  }

  @override
  void dispose(){
    super.dispose();
    _controller.dispose();
  }

}
