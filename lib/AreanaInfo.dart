import 'package:flutter/material.dart';

class ArenaInfo extends StatelessWidget {
  ArenaInfo(this.data);

  final data;
  //Detail page


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data['name']),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              _showImage(),
              _addSportUi(),
              _addTimeUi(),
              _openFor(),
              _addTimeSizeUi(),
              _addSlotCost(),
            ],
          ),
        ),
      ),
    );

  }

  Widget _showImage(){

    if(data['images'] == null){
      return _noImageFoundUi();
    }else{
      return _addImageUi();
    }

  }

  Widget _addImageUi() {
    return Padding(padding: EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _firstImage(),
            _secondImage(),
            _thirdImage()
          ],
        ),
    );
  }

 Widget _firstImage() {
    return Container(
      height: 90,
      width: 90,
      decoration: BoxDecoration(
          image: new DecorationImage(
            image: NetworkImage(data['images'][0]),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.all(Radius.circular(45.0)),
          border: new Border.all(
              color: Colors.blueAccent,
              width: 4.0
          )
      ),
    );
 }

 Widget _secondImage() {
   return Container(
     height: 90,
     width: 90,
     decoration: BoxDecoration(
         image: new DecorationImage(
           image: NetworkImage(data['images'][1]),
           fit: BoxFit.cover,
         ),
         borderRadius: BorderRadius.all(Radius.circular(45.0)),
         border: new Border.all(
             color: Colors.blueAccent,
             width: 4.0
         )
     ),
   );
 }

 Widget _thirdImage() {
   return Container(
     height: 90,
     width: 90,
     decoration: BoxDecoration(
         image: new DecorationImage(
           image: NetworkImage(data['images'][2]),
           fit: BoxFit.cover,
         ),
         borderRadius: BorderRadius.all(Radius.circular(45.0)),
         border: new Border.all(
             color: Colors.blueAccent,
             width: 4.0
         )
     ),
   );
 }

 Widget _noImageFoundUi() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 90,
        width: double.infinity,
        child: Center(
          child: Text('No Images Found',
          style: TextStyle(
            color: Colors.redAccent,
            fontWeight: FontWeight.bold,
            fontSize: 17.0
          ),),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
         borderRadius: BorderRadius.all(Radius.circular(15.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 6,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),

      ),
    );
 }


 Widget _addSportUi() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 6,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: CircleAvatar(
                backgroundColor: Colors.blueGrey,
                backgroundImage: NetworkImage(data['sports']['iconWhiteUrl']),
              ),
            ),
            Text(data['sports']['name'],
              style: TextStyle(
                  fontFamily: 'Roboto',
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.italic,
                  fontSize: 25.0
              ),
            ),
          ],

        ),
      ),
    );
 }

 Widget _addTimeUi() {
    String opentime = data['openTime'];
    String closeTime = data['closeTime'];
    return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 50,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 6,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
            ),
            child: Center(
              child: Text(" We Are open on $opentime am to $closeTime pm",
                style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.italic,
                    fontSize: 17.0
                ),
              ),
            ),
          ),
        );
 }

 Widget _addTimeSizeUi() {
    String time = data['slotTimeSize'].toString();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 6,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: Text("Slot Time Size Is : $time",
            style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.blueGrey,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic,
                fontSize: 17.0
            ),
          ),
        ),
      ),
    );
 }

 Widget _openFor(){
   int count =  data['dayOfWeeksOpen'].length;
   int no = 65;
   int total = count*no;
   double height = total.toDouble();
    return Container(
      height: height,
        width: double.infinity,
        child:ListView.builder(
            physics: NeverScrollableScrollPhysics(),
        itemCount: count,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 6,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
            ),
              child: Center(
                child: new Text(data['dayOfWeeksOpen'][index],
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.italic,
                      fontSize: 17.0
                  ),),
              ),
            ),
          );
        }));

 }

  Widget _addSlotCost() {
    String cost = data['costPerSlot'].toString();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 6,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: Text("Cost Per Slot Is : $cost",
            style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.blueGrey,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic,
                fontSize: 17.0
            ),
          ),
        ),
      ),
    );
  }


}

