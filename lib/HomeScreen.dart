import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:assignment_app/AreanaInfo.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  //API Call

  String url = 'http://playonnuat-env.eba-ernpdw3w.ap-south-1.elasticbeanstalk.com/api/v1/establishment/test/list?offset=0&limit=10';
  List data;
  Future<String> addRequest() async {
    var response = await http.get(Uri.encodeFull(url));

    setState(() {
      var extractdata =jsonDecode(response.body);
      data = extractdata;
    });


  }

  @override
  void initState() {
    this.addRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My List'),
      ),
      body: ListView.builder(
        itemCount: data == null ? 0 : data.length,
      itemBuilder: (BuildContext context, index){
          return Padding(
            padding: const EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 2.0),
            child: Card(
              elevation: 4.0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: NetworkImage(data[index]['sports']['iconBlackUrl']),
                ),

                title: Text(data[index]['name']),
                subtitle: Text(data[index]['sports']['name']),

                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ArenaInfo(data[index])));
                },
              ),
            ),
          );
      }
      )
    );
  }
}


