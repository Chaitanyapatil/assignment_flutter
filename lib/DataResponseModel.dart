  class DataResponseModel {
  String _createOn;
  String _updatedOn;
  int _id;
  List<String> _images;
  String _files;
  String _name;
  List<String> _dayOfWeeksOpen;
  String _openTime;
  String _closeTime;
  int _slotTimeSize;
  int _costPerSlot;
  String _active;
  String _establishment;
  Sports _sports;

  DataResponseModel(
      {String createOn,
      String updatedOn,
      int id,
      List<String> images,
      String files,
      String name,
      List<String> dayOfWeeksOpen,
      String openTime,
      String closeTime,
      int slotTimeSize,
      int costPerSlot,
      String active,
      String establishment,
      Sports sports}) {
    this._createOn = createOn;
    this._updatedOn = updatedOn;
    this._id = id;
    this._images = images;
    this._files = files;
    this._name = name;
    this._dayOfWeeksOpen = dayOfWeeksOpen;
    this._openTime = openTime;
    this._closeTime = closeTime;
    this._slotTimeSize = slotTimeSize;
    this._costPerSlot = costPerSlot;
    this._active = active;
    this._establishment = establishment;
    this._sports = sports;
  }

  String get createOn => _createOn;
  set createOn(String createOn) => _createOn = createOn;
  String get updatedOn => _updatedOn;
  set updatedOn(String updatedOn) => _updatedOn = updatedOn;
  int get id => _id;
  set id(int id) => _id = id;
  List<String> get images => _images;
  set images(List<String> images) => _images = images;
  String get files => _files;
  set files(String files) => _files = files;
  String get name => _name;
  set name(String name) => _name = name;
  List<String> get dayOfWeeksOpen => _dayOfWeeksOpen;
  set dayOfWeeksOpen(List<String> dayOfWeeksOpen) =>
      _dayOfWeeksOpen = dayOfWeeksOpen;
  String get openTime => _openTime;
  set openTime(String openTime) => _openTime = openTime;
  String get closeTime => _closeTime;
  set closeTime(String closeTime) => _closeTime = closeTime;
  int get slotTimeSize => _slotTimeSize;
  set slotTimeSize(int slotTimeSize) => _slotTimeSize = slotTimeSize;
  int get costPerSlot => _costPerSlot;
  set costPerSlot(int costPerSlot) => _costPerSlot = costPerSlot;
  String get active => _active;
  set active(String active) => _active = active;
  String get establishment => _establishment;
  set establishment(String establishment) => _establishment = establishment;
  Sports get sports => _sports;
  set sports(Sports sports) => _sports = sports;

  DataResponseModel.fromJson(Map<String, dynamic> json) {
    _createOn = json['createOn'];
    _updatedOn = json['updatedOn'];
    _id = json['id'];
    _images = json['images'].cast<String>();
    _files = json['files'];
    _name = json['name'];
    _dayOfWeeksOpen = json['dayOfWeeksOpen'].cast<String>();
    _openTime = json['openTime'];
    _closeTime = json['closeTime'];
    _slotTimeSize = json['slotTimeSize'];
    _costPerSlot = json['costPerSlot'];
    _active = json['active'];
    _establishment = json['establishment'];
    _sports =
        json['sports'] != null ? new Sports.fromJson(json['sports']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createOn'] = this._createOn;
    data['updatedOn'] = this._updatedOn;
    data['id'] = this._id;
    data['images'] = this._images;
    data['files'] = this._files;
    data['name'] = this._name;
    data['dayOfWeeksOpen'] = this._dayOfWeeksOpen;
    data['openTime'] = this._openTime;
    data['closeTime'] = this._closeTime;
    data['slotTimeSize'] = this._slotTimeSize;
    data['costPerSlot'] = this._costPerSlot;
    data['active'] = this._active;
    data['establishment'] = this._establishment;
    if (this._sports != null) {
      data['sports'] = this._sports.toJson();
    }
    return data;
  }
}

class Sports {
  String _createOn;
  String _updatedOn;
  int _id;
  String _name;
  String _iconWhiteUrl;
  String _iconBlackUrl;

  Sports(
      {String createOn,
      String updatedOn,
      int id,
      String name,
      String iconWhiteUrl,
      String iconBlackUrl}) {
    this._createOn = createOn;
    this._updatedOn = updatedOn;
    this._id = id;
    this._name = name;
    this._iconWhiteUrl = iconWhiteUrl;
    this._iconBlackUrl = iconBlackUrl;
  }

  String get createOn => _createOn;
  set createOn(String createOn) => _createOn = createOn;
  String get updatedOn => _updatedOn;
  set updatedOn(String updatedOn) => _updatedOn = updatedOn;
  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get iconWhiteUrl => _iconWhiteUrl;
  set iconWhiteUrl(String iconWhiteUrl) => _iconWhiteUrl = iconWhiteUrl;
  String get iconBlackUrl => _iconBlackUrl;
  set iconBlackUrl(String iconBlackUrl) => _iconBlackUrl = iconBlackUrl;

  Sports.fromJson(Map<String, dynamic> json) {
    _createOn = json['createOn'];
    _updatedOn = json['updatedOn'];
    _id = json['id'];
    _name = json['name'];
    _iconWhiteUrl = json['iconWhiteUrl'];
    _iconBlackUrl = json['iconBlackUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createOn'] = this._createOn;
    data['updatedOn'] = this._updatedOn;
    data['id'] = this._id;
    data['name'] = this._name;
    data['iconWhiteUrl'] = this._iconWhiteUrl;
    data['iconBlackUrl'] = this._iconBlackUrl;
    return data;
  }
}
