import 'dart:async';

import 'package:assignment_app/HomeScreen.dart';
import 'package:assignment_app/PlayVideo.dart';
import 'package:flutter/material.dart';


class Splash_Screen extends StatefulWidget {
  @override
  _Splash_ScreenState createState() => _Splash_ScreenState();
}

class _Splash_ScreenState extends State<Splash_Screen> {

  @override
  void initState(){
    Timer(Duration(seconds: 5), () =>
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()),
    )
    );
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
        color: Colors.white,
        child: PlayVideo());
  }
}
